library work;
    use work.all;                                           

library ieee;
    use ieee.std_logic_1164.all;                            

entity e_ParityGenerator is
    port (
        txdat : in std_logic_vector( 3 downto 0 );
        dataOut : out std_logic_vector( 4 downto 0 );
        clk : in std_logic
    );
end entity e_ParityGenerator;

architecture a_ParityGenerator of e_ParityGenerator is              
	signal dataInternal : std_logic_vector(3 downto 0);
	signal dataOutInternal : std_logic_vector(4 downto 0);          
begin
    
	LOGIC:
	process (dataInternal) is
	begin
		dataOutInternal <= dataInternal & (txdat(3) XOR txdat(2) XOR txdat(1) XOR txdat(0));
	end process LOGIC;


    GENERATOR: 
    process ( txdat, clk ) is
    begin
    if clk = '1' AND clk'event then
	dataInternal <= txdat;
	dataOut <= dataOutInternal;
    end if;
    end process GENERATOR;
        
end architecture a_ParityGenerator;