library ieee;
    use ieee.std_logic_1164.all;                            

entity e_transmissionChecker is   
    port(
        rxdat : out std_logic_vector( 3 downto 0 ); -- empfangene Daten
        ok : out std_logic; -- Empfangsstatus
        txdat : in std_logic_vector( 3 downto 0 ); -- zu übertragende Daten
        clk : in std_logic; -- Takt
        nres : in std_logic
    );
end entity e_transmissionChecker;


architecture a_transmissionChecker of e_transmissionChecker is          
    signal data : std_logic_vector(4 downto 0);

	component PG is
	port (
        txdat : in std_logic_vector( 3 downto 0 );
        dataOut : out std_logic_vector( 4 downto 0 );
        clk : in std_logic;
		reset : in std_logic
    );
	end component  PG;
	for all : PG use entity work.e_ParityGenerator;

	component PC is 
	port(
		dataIn : in std_logic_vector(4 downto 0);
        rxdat : out std_logic_vector(3 downto 0);
        clk : in std_logic;
        reset : in std_logic;
        ok : out std_logic
	);
	end component  PC;
	for all : PC use entity work.e_ParityChecker;

    begin
    pg_k : PG port map(txdat => txdat, dataOut => data, clk => clk, reset => nres);
    pc_k : PC port map(dataIn => data, rxDat => rxDat, clk => clk, reset => nres, ok => ok);
end architecture a_transmissionChecker;