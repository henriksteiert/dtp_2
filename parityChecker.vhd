library work;
    use work.all;                                           

library ieee;
    use ieee.std_logic_1164.all;                            

entity e_ParityChecker is
    port (
        dataIn : in std_logic_vector(4 downto 0);
        rxdat : out std_logic_vector(3 downto 0);
	clk : in std_logic;
        reset : in std_logic;
        ok : out std_logic
    );
end entity e_ParityChecker;

architecture a_ParityChecker of e_ParityChecker is   
	signal dataInternal : std_logic_vector(3 downto 0);
	signal dataOutInternal : std_logic_vector(3 downto 0);
	signal parityInternal : std_logic;
	signal supposedParityInternal : std_logic;
	signal resetInternal : std_logic;
	signal okInternal : std_logic;
                     
begin

	CALC:
	process (dataInternal, supposedParityInternal, resetInternal) is
	begin
		if resetInternal = '0' then
			okInternal <= '0';
			dataOutInternal <= (others => '0');
		else 
			dataOutInternal <= dataInternal;
			okInternal <= dataInternal(3) XOR dataInternal(2) XOR dataInternal(1) XOR dataInternal(0);
		end if;
	end process CALC;
	
    
    CHECKER: 
    process ( dataIn, clk, reset ) is
    variable checkedParity : std_logic;
    variable dataOut : std_logic_vector( 3 downto 0 );
    begin
	if clk = '1' AND clk'event then
	dataInternal <= dataIn(4 downto 1);
	supposedParityInternal <= dataIn(0);
	resetInternal <= reset;

	rxdat <= dataOutInternal;
	ok <= okInternal;
    	end if;
	end process CHECKER;
        
end architecture a_ParityChecker;